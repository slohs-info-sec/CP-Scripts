#!/bin/bash
# Copyright (c) Ganden Schaffner

### Functions
# This function was authored according to the mentioned link.
ask() {
	# https://djm.me/ask
	local prompt default reply

	while true; do

		if [ "${2:-}" = "Y" ]; then
			prompt="Y/n"
			default=Y
		elif [ "${2:-}" = "N" ]; then
			prompt="y/N"
			default=N
		else
			prompt="y/n"
			default=
		fi

		# Ask the question (not using "read -p" as it uses stderr not stdout)
		echo -n "$1 [$prompt] "

		# Read the answer (use /dev/tty in case stdin is redirected from somewhere else)
		read reply </dev/tty

		# Default?
		if [ -z "$reply" ]; then
			reply=$default
		fi

		# Check if the reply is valid
		case "$reply" in
			Y*|y*) return 0 ;;
			N*|n*) return 1 ;;
		esac

	done
}

# Functions related to the HTML page
write_html_page() {
	# The cat line below and the _EOF_ at the end of the function uses _EOF_ as a here tag.
	# In effect, everything between the cat line and the _EOF_ line is treated as a multiline string.
	# cat simply takes this multiline string (after running the referenced functions) and
	cat <<- _EOF_
	<html>
		<head>
		<title>$TITLE</title>
		</head>

		<body>
		<h1>$TITLE</h1>
		<p>$TIME_STAMP</p>
		$(show_uptime)
		$(drive_space)
		$(home_space)
		$(user_groups)
		$(find_media)
		$(process_list)
		</body>
	</html>
_EOF_
}

show_uptime() {
	echo "<h2>System Uptime</h2>"
	# The <pre> HTML tag is used to format the output from uptime as monospaced (as a code block).
	echo "<pre>"
	uptime
	echo "</pre>"
}

drive_space() {
	echo "<h2>Filesystem Space</h2>"
	echo "<pre>"
	df
	echo "</pre>"
}

home_space() {
	echo "<h2>Home Directory Space by User</h2>"
	echo "<pre>"
	format="%8s%10s%10s   %-s\n"
	printf "$format" "Dirs" "Files" "Blocks" "Directory"
	printf "$format" "----" "-----" "------" "---------"
	# Note that [ refers to test. [[ is a more featured version of [.
	# The semicolon is present because the then statement must be on a different line than the if statement.
	if [ $EUID -eq 0 ]; then {
		# /home/* gives the
		dir_list="/home/*"
	} else {
		# $HOME is the current user's home directory.
		dir_list=$HOME
	} fi
	for home_dir in $dir_list; do
		# find with -type d lists each directory in $home_dir on a new line.
		# wc -l counts the number of newline characters (= number lines) in this output.
		total_dirs=$(find "$home_dir" -type d | wc -l)
		# Similar to above, but -type f lists individual files instead of directories.
		total_files=$(find "$home_dir" -type f | wc -l)
		# du stands for disk usage. The command below sums the usage of files in $home_dir
		total_blocks=$(du -s "$home_dir")
		printf "$format" "$total_dirs" "$total_files" "$total_blocks"
	done
	echo "</pre>"
}

user_groups() {
	echo "<h2>Users in Special Groups</h2>"
	echo "<pre>"
	echo "Members of group 'adm':"
	# grep /etc/group for 'adm'. Take the returned line and cut it with the delimiter (-d) :. We only want the fourth field (-f).
	# (See structure of /etc/group to better understand the use of -f here.)
	grep adm /etc/group | cut -d ':' -f 4
	echo "Members of group 'root':"
	grep root /etc/group | cut -d ':' -f 4
	echo "Members of group 'sudo':"
	grep sudo /etc/group | cut -d ':' -f 4
	echo "</pre>"
}

find_media() {
	echo "<h2>Media Files</h2>"
	echo "<pre>"
	# find /home/ gives a list of files in the /home/ folder, recursively (in subfolders too).
	# This output is piped to the egrep command, where the output is searched/filtered for lines containing
	# the RegEx pattern given. The pattern reads: any number of wildcards, followed by '.', followed by one of
	# the listed file extensions.
	find /home/ | grep -E ".*\\.(jpg|tif|png|gif|wav|mp3|ogg|flac|wma|aac|m4a|m4b|flv|webm|ogv|gif|gifv|avi|wmv|mp4|mpg|3gp)"
	echo "</pre>"
}

process_list() {
	echo "<h2>Running Services</h2>"
	echo "<pre>"
	service --status-all
	echo "</pre>"
}

# Functions unrelated to the HTML page

check_UID_0() {
	# Only the root account should have UID 0. If other accounts have this UID, there's an issue.
	# If 'x:0:' occurs on > 1 lines in /etc/passwd
	if [[ $(grep -c 'x:0:' /etc/passwd) -gt 1 ]]; then {
		echo "$(tput setaf 1)There are $(grep -c 'x:0:' /etc/passwd) users with UID 0!$(tput sgr0)"
		# Read the user input in response to the prompt.
		if ! ask "Continue?" N; then {
			# Exit with code 1 (notates error typically)
			exit 1
		} fi
	} fi
}

set_users() {
	# Normal users
	# This loop will run the enclosed adduser and gpasswd commands for each line (each user) in the file.
	# The most important part of this loop is the redirect on the done line.
	# This redirects the file userlist into the loop, meaning that the read command on the first line will read from the file userlist.
	while read -r line; do
		# If the user already exists, adduser will do nothing.
		adduser "$line"
		# Remove (-d is delete) $user from group sudo
		gpasswd -d "$line" sudo
	done < userlist

	# Admininstrators
	while read -r line; do
		adduser "$line"
		# Make sure $admin is an admin by adding them to the group sudo (-a is append)
		usermod -a -G sudo "$line"
	done < adminlist

	# Delete local users who should not be present
	while read -r localuser; do # for each local human user (found using the cut | grep -E | cut)
		found=false
		while read -r user; do
			if [[ "$user" = "$localuser" ]]; then
				found=true
			fi
		done < userlist
		while read -r user; do
			if [[ "$user" = "$localuser" ]]; then
				found=true
			fi
		done < adminlist
		if [[ ! $found ]]; then
			if ask "Delete user $localuser?" N; then
				deluser "$localuser"
			fi
		fi
	# Extract human users from /etc/passwd (See: https://askubuntu.com/questions/257421/list-all-human-users)
	done < "$(cut -d: -f1,3 /etc/passwd | grep -E ':[0-9]{4}$' | cut -d: -f1)"
}

set_passwords() {
	# echo -n prevents a trailing newline from being echoed.
	echo -n "Enter a password for all users in userlist (Remember to include an uppercase char, lowercase char, digit, and symbol): "
	# Reads the input and saves it in the variable $password
	read -r password
	while read -r line; do
		echo "$line:$password" | chpasswd
		echo # Echoes a newline
		echo "User $line's password was changed to $password"
	done < userlist

	if ask "Update admin passwords?" N; then
		if ask "Be sure that the logon user is NOT in adminlist. Would you like to continue?"; then
			while read -r line; do
				echo "$line:$password" | chpasswd
				echo
				echo "Admin $line's password was changed to $password"
			done < adminlist
		fi
	fi
}

set_update_settings() {
	# These are the recommended settings set in software-properties-gtk.
	# echo "spits out" (redirects) text into the file defined by $apt_config
	# Note that the first echo uses >, while the others use >> (to append instead of overriding $apt_config)
	apt_config=/etc/apt/apt.conf.d/10periodic
	echo "APT::Periodic::Update-Package-Lists \"1\";" > $apt_config
	echo "APT::Periodic::Download-Upgradeable-Packages \"1\";" >> $apt_config
	echo "APT::Periodic::AutocleanInterval \"0\";" >> $apt_config
	echo "APT::Periodic::Unattended-Upgrade \"1\";" >> $apt_config
	echo "Apt update settings were set"
}

set_password_policy() {
	# Set minimum password length in /etc/pam.d/common-password
	# grep -q (q means quiet) will exit with zero status if any match is found. This means that the if will trigger if a match is found in the file.
	if grep -qe 'minlen=[[:digit:]]*' /etc/pam.d/common-password; then
		# sed is used for manipulating text, -i specifies to perform edits in place, writing over the specified file.
		# If an extension is attatched to -i, sed will make a backup with that extension before editing.
		# s notes string substitution. The slashes are used to seperate the different text fields.
		# A slash is conventionally used as the delimiter with sed. However, when one is using the "s" command of sed, the delimiter is taken as whatever character falls after 's'.
		# Replaces matches to the regular expression 'minlen=[[:digit:]]*' with 'minlen=12' for every instance of the search string in the file.
		# [[:digit:]] matches any single numeric character from 0 to 9. [[:digit:]] is equivalent to '[0-9]'.
		# * applies to the preceding 'token' (which was [[:digit:]] in this example), and will match any number of characters fitting this token.
		# In effect, this reads: everywhere the file /etc/pam.d/common-password has 'minlen={somenumberofnumericcharacters}, replace this with 'minlen=12'.
		# One thing to note, however, is that sed will (by default) only replace the first match to a search in each line.
		# g makes sed replace every instance of the search string in the file, not only the first in each line.
		# g is used here because I am unsure whether the minlen set matters in the 'pam_cracklib.so' line or in the 'pam_unix.so' line. (TODO)
		sed -i.bak 's/minlen=[[:digit:]]*/minlen=12/g' /etc/pam.d/common-password
	else
		# The wildcard character, '.', matches any character except line breaks (newlines).
		# '\' is used to escape the '.' in the RegExp to match. This prevents this '.' from being interpreted as the wildcard character.
		# With sed, '&' refers to the pattern found by the search. So we replace what was found by the search with itself, and then with a space and 'minlen=12'.
		sed -i.bak 's/pam_cracklib\.so.*/& minlen=12/' /etc/pam.d/common-password
		# This is done for both the 'pam_cracklib.so' and the 'pam_unix.so' line, as I am unsure which line the minlen set matters in. (TODO)
		# A backup is not made here as the substitution above already made one.
		sed -i 's/pam_unix\.so.*/& minlen=12/' /etc/pam.d/common-password
	fi

	# Set password history enforcement
	if grep -qe 'remember=[[:digit:]]*' /etc/pam.d/common-password; then
		# A backup is not made here as the substitution above already made one.
		sed -i 's/remember=[[:digit:]]*/remember=5/g' /etc/pam.d/common-password
	else
		sed -i 's/pam_unix\.so.*/& remember=5/' /etc/pam.d/common-password
	fi

	# Set password complexity requirements in /etc/pam.d/common-password
	# ucredit is uppercase, lcredit is lowercase, dcredit is digits, ocredit is other characters (symbols).
	# Setting all of these to -1 means that at least one of each is required to be present in passwords.
	sed -i 's/pam_cracklib\.so.*/& ucredit=-1 lcredit=-1 dcredit=-1 ocredit=-1/' /etc/pam.d/common-password

	# Set the password time constraints in /etc/login.defs
	# -e is used to tell sed one wants to run multiple commands. This single sed command is running three substitutions.
	# The m flag makes sed treat each line of the file as an individual string.
	# This allows one to use ^ and $ to match the start and end of each line, respectively.
	# \s refers to a whitespace character (space, tab, or newline)
	# \t refers to the tab character.
	sed -i.bak -e 's/^PASS_MAX_DAYS\s[[:digit:]]*$/PASS_MAX_DAYS\t30/m' -e 's/^PASS_MIN_DAYS\s[[:digit:]]*$/PASS_MIN_DAYS\t7/m' -e 's/^PASS_WARN_AGE\s[[:digit:]]*$/PASS_WARN_AGE\t14/m' /etc/login.defs

	# Set account lockout policy in /etc/pam.d/common-auth
	# By notting this (with !), the contents of the if block execute if 'pam_tally2.so' is not found in the file.
	if ! grep -q 'pam_tally2.so' /etc/pam.d/common-auth; then
		# \t works as the tab character in sed substitutions, but not in echo.
		# Literal tabs are used instead here.
		echo 'auth	required			pam_tally2.so deny=5 onerr=fail unlock_time=1800' >> /etc/pam.d/common-auth
	else
		sed -i.bak "s/pam_tally2\\.so.*/& deny=5 onerr=fail unlock_time=180/" /etc/pam.d/common-auth
	fi
}

disable_ssh_root_login() {
	# [[ -f /file/path ]] returns true if the file exists.
	if [[ -f /etc/ssh/sshd_config ]]; then {
		# .* refers to any number of wildcard characters (a single wildcard character is '.').
		# Replaces '^PermitRootLogin .*$' with "PermitRootLogin no" for every instance of the search string in the file.
		sed -i 's/^PermitRootLogin .*$/PermitRootLogin no/m' /etc/ssh/sshd_config
	} else {
		echo "No SSH server detected so nothing changed"
	} fi
	echo "Disabled SSH root login"
}

remove_hacking_tools() {
	echo "$(tput setaf 2)Removing hacking tools$(tput sgr0)"
	# This attempts to remove any of the tools in the list below, if they are present.
	# Backslashes are used to allow a command to continue on to the next line.
	apt-get autoremove --purge --force-yes -y airbase-ng aircrack-ng acccheck ace-voip amap apache-users arachni android-sdk apktool \
		arduino armitage asleap automater backdoor-factory bbqsql bed beef bing-ip2hosts binwalk blindelephant bluelog bluemaho bluepot \
		blueranger bluesnarfer bulk-extractor bully burpsuite braa capstone casefile cdpsnarf cewl chntpw cisco-auditing-tool \
		cisco-global-exploiter cisco-ocs cisco-torch cisco-router-config cmospwd cookie-cadger commix cowpatty crackle creddump crunch \
		cryptcat cymothoa copy-router-config cuckoo cutycapt davtest dbd dbpwaudit dc3dd ddrescue deblaze dex2jar dff dhcpig dictstat \
		dirb dirbuster distorm3 dmitry dnmap dns2tcp dnschef dnsenum dnsmap dnsrecon dnstracer dnswalk doona dos2unix dotdotpwn dradis \
		dumpzilla eapmd5pass edb-debugger enum4linux enumiax exploitdb extundelete fern-wifi-cracker fierce fiked fimap findmyhash \
		firewalk fragroute foremost funkload galleta ghost-fisher giskismet grabber go-lismero goofile gpp-decrypt gsad gsd gqrx guymager \
		gr-scan hamster-sidejack hash-identifier hexinject hexorbase http-tunnel httptunnel hping3 hydra iaxflood inguma intrace \
		inundator inviteflood ipv6-toolkit iphone-backup-analyzer intersect ismtp isr-evilgrade jad javasnoop jboss-autopwn jd-gui john \
		johnny joomscan jsql kalibrate-rtl keepnote killerbee kismet keimpx linux-exploit-suggester ldb lynis maltego-teeth magictree \
		masscan maskgen maskprocessor mdk3 metagoofil metasploit mfcuk mfoc mfterm miranda mitmproxy multiforcer multimon-ng ncrack \
		netcat nishang nipper-ng nmap ntop oclgausscrack ohwurm ollydpg openvas-administrator openvas-cli openvas-manager openvas-scanner \
		oscanner p0f padbuster paros parsero patator pdf-parser pdfid pdgmail peepdf phrasendrescher pipal pixiewps plecost polenum \
		policygen powerfuzzer powersploit protos-sip proxystrike pwnat rcrack rcrack-mt reaver rebind recon-ng redfang regripper \
		responder ridenum rsmangler rtlsdr-scanner rtpbreak rtpflood rtpinsertsound rtpmixsound sakis3g sbd sctpscan setoolkit sfuzz \
		shellnoob sidguesser siparmyknife sipp sipvicious skipfish slowhttptest smali smtp-user-enum sniffjoke snmpcheck spooftootph \
		sslcaudit sslsplit sslstrip sslyze sqldict sqlmap sqlninja sqlsus statprocessor t50 termineter thc-hydra thc-ipv6 thc-pptp-bruter \
		thc-ssl-dos tnscmd10g truecrack theharverster tlssled twofi u3-pwn uatester urlcrazy uniscan unix-privesc-check vega w3af \
		webscarab webshag webshells webslayer websploit weevely wfuzz wifi-honey wifitap wifite wireshark winexe wpscan wordlists valgrind \
		volatility voiphopper wol-e xspy xplico xsser yara yersinia zaproxy
}

### Main
# Parse the command line arguments. See https://gist.github.com/cosimo/3760587
OPTS=`getopt -o vhns: --long verbose,dry-run,help,stack-size: -n 'parse-options' -- "$@"`

if [ $? != 0 ]; then
	# >&2 redirects the echo to stderr
	echo "Failed parsing options." >&2
	exit 1
fi

echo "$OPTS"
eval set -- "$OPTS"

DRY_RUN=false

while true; do
	case "$1" in
		--dry-run ) DRY_RUN=true; shift ;;
		-- ) shift; break ;;
		* ) break ;;
	esac
done

# $EUID is the effective user ID. Using [[ ]] and -ne, we check if it is not equal to zero. Only root has $EUID = 0.
if [[ $EUID -ne 0 ]]; then {
	echo "$(tput setaf 1)This script must be run with root permissions.$(tput sgr0)"
	exit 1
} else {
	echo "This script is running as root. Let's proceed!"
} fi

if ! [[ $DRY_RUN ]] && ! ask "Have you answered the forensics questions yet?" N; then
	# Stop the script if the user has not yet answered the forensics questions
	echo "$(tput setaf 1)Answer the FQs before running this script. It may remove vulns relating to the FQs.$(tput sgr0)"
	exit 1
fi

# tput is used here to change the color of the output.
# tput setaf sets the color, while tput sgr0 resets it.
echo "$(tput setaf 5)Printing before-running HTML page$(tput sgr0)"
write_html_page > before-running.html

echo "$(tput setaf 2)Checking that only root has UID 0$(tput sgr0)"
check_UID_0

if ! [[ $DRY_RUN ]]; then
	echo "$(tput setaf 2)Setting up users and passwords$(tput sgr0)"
	# [[ ! -s /file/path ]] returns true if the file does not exist or is empty.
	if [[ ! -s userlist ]]; then
		if ! ask "userlist is empty. Proceed?" N; then
			exit 1
		fi
	elif [[ ! -s adminlist ]]; then
		if ! ask "adminlist is empty. Proceed?" N; then
			exit 1
		fi
	else
		set_users
		set_passwords
	fi

	echo "$(tput setaf 2)Setting update settings$(tput sgr0)"
	set_update_settings

	if ask "Do updates now?"; then {
		echo "$(tput setaf 2)Updating...$(tput sgr0)"
		apt-get update --force-yes -y
		echo "$(tput setaf 2)Upgrading...$(tput sgr0)"
		apt-get upgrade --force-yes -y
		echo "$(tput setaf 2)Updates are done!$(tput sgr0)"
	} fi

	if ask "Install CrackLib and configure password policy now?"; then {
		echo "$(tput setaf 2)Installing CrackLib...$(tput sgr0)"
		apt-get install libpam-cracklib --force-yes -y
		echo "$(tput setaf 2)Finished installing CrackLib. Configuring password policy...$(tput sgr0)"
		set_password_policy
		echo "$(tput setaf 2)Password policy set!$(tput sgr0)"
	} fi

	echo "$(tput setaf 2)Disabling SSH root login$(tput sgr0)"
	disable_ssh_root_login

	echo "$(tput setaf 2)Removing common hacking tools$(tput sgr0)"
	remove_hacking_tools
fi

exit 0
