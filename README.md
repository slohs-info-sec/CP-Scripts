# CP-Scipts
Helpful tools for basic CyberPatriot tasks.  
Comments are fairly liberal for teaching/learning purposes.

## Ubuntu
*It is likely against CyberPatriot rules for you to use this script, as you are not its author.*

### Usage
```
git clone https://gitlab.com/slohs-info-sec/CP-Scripts.git
cd CP-Scripts
sudo chmod 755 *
# Fill userlist with the names of non-admin users as specified in the README. One per line.
nano userlist
# Fill adminlist with the names of admin users as specified in the README. One per line.
# Be sure to not place the README's listed passwords in adminlist.
# DO NOT INCLUDE YOUR USER (the auto-login user).
nano adminlist
sudo ./ubuntu.sh 2>&1 | tee output.log
```

* `sudo ./ubuntu.sh --dry-run 2>&1 | tee output.log` may be used instead to generate the HTML output page only. This is particularly useful for identifying media files.

### Features
* Checks that only root has UID 0
* Adds users and sets normal/admin permissions based on userlist and adminlist
* Deletes users not in userlist/adminlist (with the user's approval)
* Sets update settings
* Carries out updates (with the user's approval)
* Installs Cracklib and configures password policy (with the user's approval)
* Disables SSH root login
* Removes common hacking tools

## License
This project is licensed under the GNU GPLv3 license. See [LICENSE](LICENSE).
